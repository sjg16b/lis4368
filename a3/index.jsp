<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Sat, 01-02-21, 18:35:01 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Spencer Goldstein">
	<link rel="icon" href="img/favicon.ico">

	<title>LIS4368 - Assignment3</title>

	<%@ include file="/css/include_css.jsp" %>		
	
</head>
<style>
.navbar-inverse {
	background-image: linear-gradient(to bottom,#28aefc 10%,#000000 100%);}
img {
	border: 3px solid #000000;
	}
</style>
<body>

<!-- display application path -->
<% //= request.getContextPath()%>
	
<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->

	<%@ include file="/global/nav.jsp" %>	

	<div class="container">
		<div class="starter-template">
					<div class="page-header">
						<%@ include file="global/header.jsp" %>
					</div>

					<b><hr>Screenshot of ERD:</b><br /><hr>
					<img src="img/erd.jpg" class="img-responsive center-block" alt="erd screenshot" />

					<br /> <br />
                    <b> MySQL Workbench and SQL Files: </b><br>
					<b><a href="docs/a3.mwb"> Petstore MySQL Workbench File</a></b><br>
                    <b><a href="docs/a3.sql"> Petstore SQL File</a></b>

					

	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>		

</body>
</html>
