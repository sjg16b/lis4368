# LIS4368 Advanced Web Applications Development

## Spencer Goldstein

### Assignment 3 Requirements:

*Deliverables:*

1. ERD 
2. Data (10 records per table)
3. Chapter Questions (ch. 7,8)

### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of a3/index.jsp
* Links to 
    - a3.mwb
    - a3.sql

#### Assignment Screenshot and Links:
*Screenshot A3 ERD*:

![A3 ERD](img/erd.jpg "ERD based upon A3 Requirements")

![Petstore Data](img/petstore.jpg "Petstore Table Data from MySQL Workbench")

![Pet Data](img/pet.jpg "Pet Table Data from MySQL Workbench")

![Customer Data](img/customer.jpg "Customer Table Data from MySQL Workbench")

*Screenshot A3 Index*:

![A3 Index](img/a3index.jpg "A3 Index page")

*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")
