# LIS4368 Advanced Web Applications Development

## Spencer Goldstein

### Project 2 Requirements:

*Deliverables:*

1. Application Screenshots
2. Chapter Questions (ch. 16-17)

### README.md file should include the following items:

* Screenshot of Valid User Entry 
* Screenshot of Passed validation
* Screenshot of Data Display
* Screenshot of Modify Form
* Screenshot of Modified Data
* Screenshot of Delete Warning
* Screenshots of Associated Database Changes

#### Assignment Screenshots:

![Valid Form](img/ValidEntry.png "Example of valid form")

![Validation Passed](img/PassedValidation.png "Example of validation passed")

![Data Display](img/DisplayData.png "Customer table with new entry")

![Modify Form](img/ModifyForm.png "Modify form")

![Modified Data](img/ModifiedData.png "modified data")

![Delete Warning](img/DeleteWarning.png "delete warning")

![Select](img/select.png "select data")

![Insert](img/insert.png "insert data")

![Update](img/update.png "update data")

![Delete](img/delete.png "delete data")
