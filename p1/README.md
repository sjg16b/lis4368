# LIS4368 Advanced Web Applications Development

## Spencer Goldstein

### Project 1 Requirements:

*Deliverables:*

1. Homepage Carousel with 3 slides
2. Validation Screenshots
3. Chapter Questions (ch. 9,10)

### README.md file should include the following items:

* Screenshot of Carousel Page
* Screenshot of Failed and Passed validation

#### Project Screenshots and Links:

![Index](img/index.png "Index with personalized slides")

![Validation Failed](img/failed_val.png "Example of validation failed")

![Validation Passed](img/passed_val.png "Example of validation passed")
