# LIS4368 Advanced Web Applications Development

## Spencer Goldstein

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. JAVA/JSP/Servlet Development Installation
3. Chapter Questions (ch. 1-4)

#### README.md file should include the following items:

* Screenshot running java Hello;
* Screenshot running http://localhost9999;
* Git commands with shirt descriptions;
* Bitbucket repo links: a) this assignment and b) the completed tutorial (bitbucketstationlocations).


#### Git commands w/short descriptions:

1. git-init - Create an empty Git repository or reinitialize an existing one
2. git-status - Show the working tree status
3. git-add - Add file contents to the index
4. git-commit - Record changes to the repository
5. git-push - Update remote refs along with associated objects
6. git-pull - Fetch from and integrate with another repository or a local branch
7. git-branch - List, create, or delete branches

#### Assignment Screenshots:

*Screenshot of running http://localhost9999*:

![tomcat Screenshot](img/tomcat.jpg)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of assignment 1*:

![assignment 1 screenshot](img/a1screenshot.jpg)

#### Assignment Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Repo Link](https://bitbucket.org/sjg16b/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*Local Web App Index Link:*
[A1 My Local Web App Index Link](http://localhost:9999/lis4368/index.jsp "My Local Web App Index")
