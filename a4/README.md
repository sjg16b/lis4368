# LIS4368 Advanced Web Applications Development

## Spencer Goldstein

### Assignment 4 Requirements:

*Deliverables:*

1. Application Screenshots
2. Skill Screenshots
3. Chapter Questions (ch. 11,12)

### README.md file should include the following items:

* Screenshot of Failed Validation
* Screenshot of Passed validation

#### Assignment Screenshots:

![Validation Failed](img/failedValidation.png "Example of validation failed")

![Validation Passed](img/passedValidation.png "Example of validation passed")
