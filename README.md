# LIS4368 Advanced Web Applications Development

## Spencer Goldstein

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Configure Distributed Version Control
    - Install AMMPS
    - Install JDK
    - Install Tomcat
    - Create Bitbucket Repository
    - Compile and Run tutorial Java Program

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MySql Workbench
    - Write a Java Servlet
    - Write a Database Servlet

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create an example database
    - Forward engineer tables using MySQL Workbench

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Use Model View Controller

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Use MVC
    - Insert data into SQl database using Web App

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Use JQuery Validation
    - Use Regular Expressions

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Culmination of all prior projects and assignments
    - JSP/Servlet Web App using MVC framework and providing CRUD functionality