<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Sat, 01-02-21, 18:35:01 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Spencer Goldstein">
	<link rel="icon" href="img/favicon.ico">

	<title>LIS4368 - Assignment2</title>

	<%@ include file="/css/include_css.jsp" %>		
	
</head>
<style>
.navbar-inverse {
	background-image: linear-gradient(to bottom,#28aefc 10%,#000000 100%);}
img {
	border: 3px solid #000000;
	}
</style>
<body>

<!-- display application path -->
<% //= request.getContextPath()%>
	
<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->

	<%@ include file="/global/nav.jsp" %>	

	<div class="container">
		<div class="starter-template">
					<div class="page-header">
						<%@ include file="global/header.jsp" %>
					</div>

					<b><hr>Screenshot of http://localhost:9999/hello:</b><br /><hr>
					<img src="img/hello.PNG" class="img-responsive center-block" alt="hello screenshot" />

					<br /> <br />
					<hr><b>Screenshot of http://localhost:9999/hello/HelloHome.html:</b><br /><hr>
					<img src="img/helloHome.PNG" class="img-responsive center-block" alt="404 screenshot" />

					<br /> <br />
					<hr><b>Screenshot of http://localhost:9999/hello/sayhello:</b><br /><hr>
					<img src="img/helloWorld.PNG" class="img-responsive center-block" alt="hello world screenshot" />

					<br /> <br />
					<hr><b>Screenshot of http://localhost:9999/hello/querybook.html (item not selected):</b><br /><hr>
					<img src="img/servletNotSelected.PNG" class="img-responsive center-block" alt="servlet not selected screenshot" />

					<br /> <br />
					<hr><b>Screenshot of http://localhost:9999/hello/querybook.html (item not selected):</b><br /><hr>
					<img src="img/servletSelected.PNG" class="img-responsive center-block" alt="servlet selected" />

					<br /> <br />
					<hr><b>Screenshot of query results:</b><br /><hr>
					<img src="img/queryResults.PNG" class="img-responsive center-block" alt="query results" />

	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>		

</body>
</html>
