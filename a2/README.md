# LIS4368 Advanced Web Applications Development

## Spencer Goldstein

### Assignment 2 Requirements:

*Two Parts:*

1. Development Environment
    - Establish connection between MySql and local server
2. Chapter Questions (ch. 5-8)

#### README.md file should include the following items:

* Assessment Links
* Bitbucket repo link to this assignment.
* Screenshot from the query results from http://localhost:9999/hello/querybook.html;

### Assignment Screenshots:

*Screenshot of http://localhost:9999/hello*:

![hello Screenshot](img/hello.PNG)

*Screenshot of http://localhost:9999/hello/HelloHome.html*:

![404 Screenshot](img/helloHome.PNG)

*Screenshot of http://localhost:9999/hello/sayhello*:

![hello world Screenshot](img/helloWorld.PNG)

*Screenshot of http://localhost:9999/hello/querybook.html*:

![servelet not selected Screenshot](img/servletNotSelected.PNG)

*Screenshot of http://localhost:9999/hello/querybook.html (item selected)*:

![servelet selected Screenshot](img/servletSelected.PNG)

*Screenshot of query results*:

![query results Screenshot](img/queryResults.PNG)

*Screenshot of assignment 2*:
![assignment 2 screenshot](img/a2screenshot.PNG)

## Assignment Links:

*Local Assessment Links*

* [A](http://localhost:9999/hello "My World")

* [B](http://localhost:9999/hello/HelloHome.html "404 page will display")

* [C](http://localhost:9999/hello/sayhello "invokes hello servlet")

* [D](http://localhost:9999/hello/querybook.html "invokes database servlet")


*Local Web App Index Link:*
[A1 My Local Web App Index Link](http://localhost:9999/lis4368/index.jsp "My Local Web App Index")
